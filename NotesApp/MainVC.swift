//
//  ViewController.swift
//  NotesApp
//
//  Created by Jigar Panchal on 3/30/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var notesCountLbl: UILabel!
   
   var notes: [Notes] = []
   
   
   override func viewDidLoad() {
      super.viewDidLoad()

      tableView.delegate = self
      tableView.dataSource = self
      
      
      
   }

   override func viewWillAppear(_ animated: Bool) {
      
      getData()

      updateCountLbl()
      
      tableView.reloadData()
      
      
   }
 
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return notes.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = UITableViewCell()
      
      let note = notes[indexPath.row]
      
      if note.isText == nil{
         return UITableViewCell()
      }else{
         cell.textLabel?.text = note.isText!
         return cell
      }
   }
   
   func getData(){
         let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

     // let request = context.fetch(Notes.fetchRequest())

      do{
            notes = try context.fetch(Notes.fetchRequest())
      }catch{
         print("Fetching Failed...!")
      }
   }
   
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      if editingStyle == .delete{
         let note = notes[indexPath.row]
         context.delete(note)
         (UIApplication.shared.delegate as! AppDelegate).saveContext()
         
         getData()
         
         updateCountLbl()
         
         tableView.reloadData()
      }
   }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let note = notes[indexPath.row]
      performSegue(withIdentifier: "NotesVC", sender: note)
      
   }
   
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "NotesVC" {
         if let destination = segue.destination as? NotesVC {
            if let notes = sender as? Notes {
               destination.itemToEdit = notes
            }
         }
      }
      
   }
   
   func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
    let cell = tableView.cellForRow(at: indexPath)
   
      let customView = UIView()
      
      customView.backgroundColor = UIColor(displayP3Red: 255/255 , green: 243/255, blue: 204/255, alpha: 0.7)
      cell?.selectedBackgroundView = customView
      
   }
   
   func updateCountLbl(){
      if notes.count == 0{
         notesCountLbl.text = "MyNotes 0"
      }else{
         notesCountLbl.text = "MyNotes \(notes.count)"
      }
   }
   
}

