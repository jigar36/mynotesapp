//
//  NotesVC.swift
//  NotesApp
//
//  Created by Jigar Panchal on 3/30/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit
import CoreData
import Speech

class NotesVC: UIViewController, SFSpeechRecognizerDelegate {

   @IBOutlet weak var textField: UITextView!
   @IBOutlet weak var recordBtn: UIButton!
   
   private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
   
   private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
   private var recognitionTask: SFSpeechRecognitionTask?
   private let audioEngine = AVAudioEngine()
   var itemToEdit: Notes?
   
   
    override func viewDidLoad() {
        super.viewDidLoad()

      textField.becomeFirstResponder()
      
      NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
      
      
      let date = Date()
      let calendar = Calendar.current
      let hour = calendar.component(.hour, from: date)
      let minutes = calendar.component(.minute, from: date)
      
      print("date is\(date)")
   //   print("calendar is\(calendar)")
      print("hour is\(hour)")
      print("min is\(minutes)")
      
      recordBtn.isEnabled = false
      
      speechRecognizer.delegate = self
      
      SFSpeechRecognizer.requestAuthorization { (authStatus) in
         
         var isButtonEnabled = false
         
         switch authStatus {
         case .authorized:
            isButtonEnabled = true
            
         case .denied:
            isButtonEnabled = false
            print("User denied access to speech recognition")
            
         case .restricted:
            isButtonEnabled = false
            print("Speech recognition restricted on this device")
            
         case .notDetermined:
            isButtonEnabled = false
            print("Speech recognition not yet authorized")
         }
         
         OperationQueue.main.addOperation() {
            self.recordBtn.isEnabled = isButtonEnabled
         }
      }
      
      if itemToEdit != nil {
         loadItemData()
      }
      
    }
   
   func textFieldShouldReturn(textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
   }
   
   func keyboardWillShow(notification:NSNotification){
      
      var userInfo = notification.userInfo!
      var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
      keyboardFrame = self.view.convert(keyboardFrame, from: nil)
      
      var contentInset:UIEdgeInsets = self.textField.contentInset
      contentInset.bottom = keyboardFrame.size.height
      self.textField.contentInset = contentInset
   }
   
   func keyboardWillHide(notification:NSNotification){
      
      let contentInset:UIEdgeInsets = UIEdgeInsets.zero
      self.textField.contentInset = contentInset
   }


   func loadItemData(){
      if itemToEdit?.isText == "New MyNote"{
      
         textField.text = ""
         
      }else{
         textField.text = itemToEdit?.isText
      }
   }
   
   
   
   @IBAction func doneBtnPressed(_ sender: Any) {
     textField.resignFirstResponder()
   }
   
   @IBAction func recordBtnPressed(_ sender: Any) {
      
      if audioEngine.isRunning {
         audioEngine.stop()
         recognitionRequest?.endAudio()
         recordBtn.isEnabled = false
         recordBtn.setTitle("Start Recording", for: .normal)
      } else {
         startRecording()
         recordBtn.setTitle("Stop Recording", for: .normal)
      }
   }
   func startRecording() {
      
      if recognitionTask != nil {  //1
         recognitionTask?.cancel()
         recognitionTask = nil
      }
      
      let audioSession = AVAudioSession.sharedInstance()  //2
      do {
         try audioSession.setCategory(AVAudioSessionCategoryRecord)
         try audioSession.setMode(AVAudioSessionModeMeasurement)
         try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
      } catch {
         print("audioSession properties weren't set because of an error.")
      }
      
      recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
      
      guard let inputNode = audioEngine.inputNode else {
         fatalError("Audio engine has no input node")
      }  //4
      
      guard let recognitionRequest = recognitionRequest else {
         fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
      } //5
      
      recognitionRequest.shouldReportPartialResults = true  //6
      
      recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
         
         var isFinal = false  //8
         
         if result != nil {
            
            self.textField.text = result?.bestTranscription.formattedString  //9
            isFinal = (result?.isFinal)!
         }
         
         if error != nil || isFinal {  //10
            self.audioEngine.stop()
            inputNode.removeTap(onBus: 0)
            
            self.recognitionRequest = nil
            self.recognitionTask = nil
            
            self.recordBtn.isEnabled = true
         }
      })
      
      let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
      inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
         self.recognitionRequest?.append(buffer)
      }
      
      audioEngine.prepare()  //12
      
      do {
         try audioEngine.start()
      } catch {
         print("audioEngine couldn't start because of an error.")
      }
      
      textField.text = "Say something, I'm listening!"
      
   }
   
   func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
      if available {
         recordBtn.isEnabled = true
      } else {
         recordBtn.isEnabled = false
      }
   }
   @IBAction func backBtnPressed(_ sender: Any) {
      
      
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      
      let notes: Notes!
      
      if itemToEdit == nil{
         
         notes = Notes(context: context)
         
      }else{
         notes = itemToEdit
      }
      
  
      if textField.text.isEmpty{
         notes.isText = "New MyNote"
         notes.created = NSDate()
         (UIApplication.shared.delegate as! AppDelegate).saveContext()
         
      }else{
      
         notes.isText = textField.text!
         notes.created = NSDate()
         (UIApplication.shared.delegate as! AppDelegate).saveContext()
      }
      
       dismiss(animated: true, completion: nil)
   }
}
