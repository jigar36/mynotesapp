# README #

Quickly capture what’s on your mind. Speak a voice memo on the go and have it automatically transcribed. MyNotesApp makes it easy to capture a thought or list for yourself, and share it with friends and family.

Capture what’s on your mind
• Add notes, lists to MyNotesApp. Pressed for time? Record a voice memo and Keep will transcribe it so you can find it later.
Share ideas with friends and family
• Easily plan that surprise party by sharing your notes with others.

Permissions Notice
Microphone: This is used to attach audio to notes.
Storage: This is used to add attachments from storage to their notes.

![Simulator Screen Shot Jun 8, 2017, 1.07.23 PM.png](https://bitbucket.org/repo/5qdebeB/images/3796242177-Simulator%20Screen%20Shot%20Jun%208,%202017,%201.07.23%20PM.png)


![Simulator Screen Shot Jun 8, 2017, 1.07.37 PM.png](https://bitbucket.org/repo/5qdebeB/images/1805637761-Simulator%20Screen%20Shot%20Jun%208,%202017,%201.07.37%20PM.png)

![Simulator Screen Shot Jun 8, 2017, 1.05.16 PM.png](https://bitbucket.org/repo/5qdebeB/images/3794309874-Simulator%20Screen%20Shot%20Jun%208,%202017,%201.05.16%20PM.png)


![Simulator Screen Shot Jun 8, 2017, 1.06.09 PM.png](https://bitbucket.org/repo/5qdebeB/images/3905033469-Simulator%20Screen%20Shot%20Jun%208,%202017,%201.06.09%20PM.png)